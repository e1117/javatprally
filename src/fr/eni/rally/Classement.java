package fr.eni.rally;

import java.util.Date;

public class Classement {

	Date cumulTemps;

	private Equipage equipage;

	public Classement(Equipage equipage, Date cumulTemps) {
		this.equipage = equipage;
		this.cumulTemps = cumulTemps;
	}

	public Date cumulTemps() {
		return cumulTemps;
	}

	public String infosEquipage() {
		return toString();
	}

	@Override
	public String toString() {
		return equipage.toString() + " cumulTemps=" + cumulTemps;
	}
}
