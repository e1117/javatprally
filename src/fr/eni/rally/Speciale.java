package fr.eni.rally;

import java.util.Date;

public class Speciale {

	private Date jourHeure;
	private double distance;
	private String nom;
	private int resultatIndex;
	private Resultat[] resultats;
	private TypeEpreuve type;

	/**
	 * Constructeur
	 * 
	 * @param nom
	 *            nom de l'épreuve
	 * @param aLieuLe
	 *            date et heure de départ de la spéciale
	 * @param distance
	 *            distance à parcourir sur l'épreuve
	 * @param type
	 *            type d'épreuve. Voir enum TypeEpreuve
	 */
	public void Specia(String nom, Date aLieuLe, double distance, TypeEpreuve type) {
		this.nom = nom;
		this.jourHeure = aLieuLe;
		this.distance = distance;
		this.type = type;
		this.resultatIndex = 0;
		this.resultats = new Resultat[50];
	}

	/**
	 * 
	 * @return nom de l'épreuve
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * Retourne le classement des equipages sur la spéciale
	 * 
	 * @return non implémenté, tableau non trié.
	 */
	public Resultat[] getClassement() {
		// TODO
		return resultats;
	}

	/**
	 * Ajoute un Resultat de concurrents à la liste des résultats de l'épreuve
	 * 
	 * @param resultat
	 *            Resultat a ajouter
	 * @throws ArrayIndexOutOfBoundsException
	 */
	public void ajouterResultat(Resultat resultat) throws ArrayIndexOutOfBoundsException {
		if (resultatIndex < 50) {
			this.resultats[resultatIndex] = resultat;
			resultatIndex++;
		} else {
			throw new ArrayIndexOutOfBoundsException("Nombre de résultats autorisés dépassés");
		}
	}

	/**
	 * Renvoie les infos à afficher
	 * 
	 * @return non implémenté, renvoie " en dev "
	 */
	public String infoSpeciale() {

		// TODO
		return " en dev ";
	}
}
