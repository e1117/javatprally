package fr.eni.rally;

import java.util.Date;

public class Rallye {
	private Date date;
	private String pays;
	private int specialeIndex;
	private Speciale[] speciales;

	/**
	 * Constructeur
	 * 
	 * @param pays
	 *            pays de départ de la course
	 * @param date
	 *            date et heure de lancement de la course
	 */
	public Rallye(String pays, Date date) {
		this.pays = pays;
		this.date = date;
		this.specialeIndex = 0;
		this.speciales = new Speciale[20];
	}

	/**
	 * Retourne une spéciale par recherche du nom
	 * 
	 * @param nom
	 *            nom exacte de la Speciale
	 * @return Speciale demandée ou null
	 */
	public Speciale getSpeciale(String nom) {
		for (int i = 0; i < speciales.length; i++) {
			if (speciales[i].getNom().equals(nom)) {
				return speciales[i];
			}
		}
		return null;
	}

	/**
	 * Ajoute la speciale à la course
	 * 
	 * @param speciale
	 *            speciale a ajouter
	 * @throws ArrayIndexOutOfBoundsException
	 */
	public void ajouterSpeciale(Speciale speciale) throws ArrayIndexOutOfBoundsException {
		if (specialeIndex < 20) {
			this.speciales[specialeIndex] = speciale;
			specialeIndex++;
		} else {
			throw new ArrayIndexOutOfBoundsException("Nombre de speciales autorisées dépassées");
		}
	}

	/**
	 * Renvoie les infos d'un rallye avec ou sans speciales
	 * 
	 * @param inclureSpeciale
	 *            true = affiche les speciales, false = affiche uniquement le rallye
	 * @return " en dev ", non implementé
	 */
	public String infosRallye(boolean inclureSpeciale) {
		// TODO
		return " en dev ";
	}

	/**
	 * Retourne le classement général du rallye par temps.
	 * 
	 * @return null, non implementé
	 */
	public Classement[] getClassementGeneral() {
		// TODO
		return null;
	}
}
