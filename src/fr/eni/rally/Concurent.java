package fr.eni.rally;

public class Concurent {

	private String nom;
	private String prenom;
	private String nat;

	public Concurent(String nom, String prenom, String nationalite) {
		this.nom = nom;
		this.prenom = prenom;
		this.nat = nationalite;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getNationalite() {
		return nat;
	}

	public String infosConcurent() {
		String infos = new String("Le concurent s'appele " + nom + " " + prenom + " est originaire de " + nat + ".");
		System.out.println(infos);
		return infos;
	}

}
