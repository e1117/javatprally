package fr.eni.rally;

import java.util.Date;

public class Resultat {

	Date temps;
	private Equipage equipage;
	private Special special;

	public Resultat(Equipage equipage, Special special, Date temps) {
		this.equipage = equipage;
		this.special = special;
		this.temps = temps;
	}
}
