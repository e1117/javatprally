package fr.eni.rally;

/**
 * 
 * @author LESTEVEN
 * 
 *         Enumeration des types de Spéciales
 */
public enum TypeEpreuve {
	QUALIF, SPECIALE, LIAISON
}